from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

from user.settings import STATIC_TEXT
from . import BaseTest

class SignUpTest(BaseTest):

    jack = dict(username='jack@gmail.com',
                first_name='jackfirstname',
                last_name='jacklastname',
                email='jack@gmail.com',
                password='12345',
                repeat_password='12345')

    black_jack = dict(username='black_jack',
                      first_name='black_jack',
                      last_name='black_jack',
                      email='black_jack',
                      password='1234567',
                      repeat_password='123456')

    def test_signup_anon_user(self):

        response = self.anon_client.get(reverse('signup'))
        self.assertTemplateUsed(response, 'user/signup.html')

    def test_signup_for_registered_user(self):

        response = self.anon_client.post(reverse('signup'), BaseTest.eduard_data)
        self.assertEqual(1, User.objects.all().count())
        self.assertContains(response, STATIC_TEXT['user_with_such_username_already_exists'])
        self.assertContains(response, STATIC_TEXT['user_with_such_email_already_exists'])

    def test_signup_for_logged_user(self):

        for method in ('get', 'post'):
            response = getattr(self.eduard_client, method)(reverse('signup'))
            self.assertRedirects(response, reverse('index'))

    def test_signup_for_new_correct_user(self):

        response = self.anon_client.post(reverse('signup'), SignUpTest.jack, follow=True)
        self.assertEqual(2, User.objects.all().count())
        self.assertTemplateUsed(response, 'user/signin.html')
        self.assertContains(response, STATIC_TEXT['successfully_registered'])

    def test_signup_for_new_incorrect_user(self):
        
        response = self.anon_client.post(reverse('signup'), {})
        self.assertEqual(1, User.objects.all().count())
        self.assertTemplateUsed(response, 'user/signup.html')
        self.assertContains(response, STATIC_TEXT['this_field_is_required'], count=6)

        response = self.anon_client.post(reverse('signup'), SignUpTest.black_jack)
        self.assertEqual(1, User.objects.all().count())
        self.assertTemplateUsed(response, 'user/signup.html')
        self.assertContains(response, STATIC_TEXT['passwords_should_match'])
