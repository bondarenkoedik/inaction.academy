from django.core.urlresolvers import reverse

from user.settings import STATIC_TEXT
from . import BaseTest


class AccountSettingsTest(BaseTest):

    jack = dict(username='jack',
                first_name='jackfirstname',
                last_name='jacklastname',
                email='jack@gmail.com',
                password='12345',
                repeat_password='12345')

    new_jack = dict(username='newjack',
                    first_name='newjackfirstname',
                    last_name='newjacklastname',
                    email='newjack@gmail.com',
                    password='123456') # password from eduard

    def test_account_settings_for_anon_user(self):

        for method in ('get', 'post'):
            response = getattr(self.anon_client, method)(reverse('account_settings'))
            self.assertRedirects(response, reverse('signin')+'?next='+reverse('account_settings'))

    def test_account_settings_for_logged_user(self):

        response = self.eduard_client.get(reverse('account_settings'))
        self.assertTemplateUsed(response, 'user/account_settings.html')

    def test_change_data_for_logged_user(self):

        response = self.eduard_client.post(reverse('account_settings'))
        self.assertContains(response, STATIC_TEXT['this_field_is_required'], count=4)

        response = self.eduard_client.post(reverse('account_settings'), AccountSettingsTest.new_jack)
        self.assertContains(response, STATIC_TEXT['changes_were_saved'])

        self.eduard_client.get(reverse('signout'))

        response = self.eduard_client.post(reverse('signin'), BaseTest.eduard_data)
        self.assertContains(response, STATIC_TEXT['incorrect_username_or_password'])

        response = self.eduard_client.post(reverse('signin'), AccountSettingsTest.new_jack)
        self.assertRedirects(response, reverse('account'))

    def test_change_similar_data_for_logged_user(self):

        self.anon_client.post(reverse('signup'), AccountSettingsTest.jack)
        self.anon_client.post(reverse('signin'), AccountSettingsTest.jack)

        response = self.anon_client.post(reverse('account_settings'), BaseTest.eduard_data)
        self.assertContains(response, STATIC_TEXT['user_with_such_email_already_exists'])
        self.assertContains(response, STATIC_TEXT['user_with_such_username_already_exists'])