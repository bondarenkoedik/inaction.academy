from django.core.urlresolvers import reverse

from . import BaseTest

class SignOutTest(BaseTest):

    def test_signout_for_anon_user(self):

        for method in ('get', 'post'):
            response = getattr(self.anon_client, method)(reverse('signout'))
            self.assertRedirects(response, reverse('index'))        

    def test_signout_for_registered_user(self):

        for method in ('get', 'post'):
            response = getattr(self.eduard_client, method)(reverse('signout'))
            self.assertRedirects(response, reverse('index'))