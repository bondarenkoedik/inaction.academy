from django.contrib.auth.models import User
from django.test import Client, TestCase

class BaseTest(TestCase):

    eduard_data = dict(
        username='eduardb',
        first_name='eduardfirstname',
        last_name='eduardlastname',
        email='eduardb@gmail.com',
        password='123456',
        repeat_password='123456',
    )

    def get_data_to_create_object(self, data):
        return {key: value for key, value in data.items() if key != 'repeat_password'}

    def setUp(self):

        self.eduard = User(**self.get_data_to_create_object(BaseTest.eduard_data))
        self.eduard.set_password(self.eduard.password)
        self.eduard.save()

        self.anon_client = Client()
        self.eduard_client = Client()

        assert self.eduard_client.login(username=BaseTest.eduard_data['username'], password=BaseTest.eduard_data['password'])    

    def tearDown(self):
        User.objects.all().delete()


