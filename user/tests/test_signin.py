from django.core.urlresolvers import reverse

from user.settings import STATIC_TEXT
from . import BaseTest

class SignInTest(BaseTest):

    jack = dict(username='jack@gmail.com',
                first_name='jackfirstname',
                last_name='jacklastname',
                email='jack@gmail.com',
                password='12345',
                repeat_password='12345')

    black_jack = dict(username='black_jack',
                      password='123')

    def test_signin_anon_user(self):

        response = self.anon_client.get(reverse('signin'))
        self.assertTemplateUsed(response, 'user/signin.html')

    def test_signin_for_registered_user(self):

        for method in ('get', 'post'):
            response = getattr(self.eduard_client, method)(reverse('signin'))
            self.assertRedirects(response, reverse('index'))

    def test_signin_after_signup(self):

        response = self.anon_client.post(reverse('signin'), BaseTest.eduard_data)
        self.assertRedirects(response, reverse('account'))

    def test_signin_for_new_correct_user(self):

        response = self.anon_client.post(reverse('signup'), SignInTest.jack)
        self.assertRedirects(response, reverse('signin'))

        response = self.anon_client.post(reverse('signin'), SignInTest.jack)
        self.assertRedirects(response, reverse('account'))

    def test_signin_for_new_incorrect_user(self):
        
        response = self.anon_client.post(reverse('signin'), {})
        self.assertTemplateUsed(response, 'user/signin.html')
        self.assertContains(response, STATIC_TEXT['this_field_is_required'], count=2)

        response = self.anon_client.post(reverse('signin'), SignInTest.black_jack)
        self.assertTemplateUsed(response, 'user/signin.html')
        self.assertContains(response, STATIC_TEXT['incorrect_username_or_password'])

