from django.core.urlresolvers import reverse

from user.settings import STATIC_TEXT
from . import BaseTest


class AccountChangePassword(BaseTest):

    jack = dict(username='jack@gmail.com',
                first_name='jackfirstname',
                last_name='jacklastname',
                email='jack@gmail.com',
                password='12345',
                repeat_password='12345')

    new_jack = dict(username='jack@gmail.com',
                    first_name='jackfirstname',
                    last_name='jacklastname',
                    email='jack@gmail.com',
                    password='-123456',
                    repeat_password='-123456')

    wrong_change_password = {'old_password': '-12345',
                       'password': '-1234567',
                       'repeat_password': '-123456'}

    change_password = {'old_password': 12345,
                       'password': '-123456', # should match new_jack password
                       'repeat_password': '-123456'}

    def test_for_anon_user_account_change_password(self):

        for method in ('get', 'post'):
            response = getattr(self.anon_client, method)(reverse('account_change_password'))
            self.assertRedirects(response, reverse('signin')+'?next='+reverse('account_change_password'))

    def test_for_logged_user_account_change_password(self):

        response = self.eduard_client.get(reverse('account_change_password'))
        self.assertTemplateUsed(response, 'user/account_change_password.html')

        response = self.eduard_client.post(reverse('account_change_password'), {})
        self.assertContains(response, STATIC_TEXT['this_field_is_required'], count=3)

    def test_for_user_account_change_password(self):

        self.anon_client.post(reverse('signup'), AccountChangePassword.jack)
        self.anon_client.post(reverse('signin'), AccountChangePassword.jack)

        response = self.anon_client.post(reverse('account_change_password'), AccountChangePassword.wrong_change_password)
        self.assertTemplateUsed(response, 'user/account_change_password.html')
        self.assertContains(response, STATIC_TEXT['passwords_should_match'])
        self.assertContains(response, STATIC_TEXT['old_password_is_not_correct'])

        response = self.anon_client.post(reverse('account_change_password'), AccountChangePassword.change_password)
        self.assertTemplateUsed(response, 'user/account_change_password.html')
        self.assertContains(response, STATIC_TEXT['password_successfully_changed'])

        self.anon_client.get(reverse('signout'))
        response = self.anon_client.post(reverse('signin'), AccountChangePassword.new_jack)
        self.assertRedirects(response, reverse('account'))
