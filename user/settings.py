# File to store all settings related to this particular app

STATIC_TEXT = dict(
    this_field_is_required='This field is required.',
    incorrect_username_or_password='Incorrect username or password.',
    account_is_disabled='Your account is disabled.',
    successfully_registered='You are successfully registered. Please, use data to login.',
    user_with_such_username_already_exists='A user with that username already exists.',
    user_with_such_email_already_exists='A user with such email already exists.',
    passwords_should_match='Passwords should match!',
    password_successfully_changed='Password successfully changed.',
    old_password_is_not_correct='Old password is not correct.',
    changes_were_saved='Changes were saved.',
)