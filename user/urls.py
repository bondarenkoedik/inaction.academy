from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^signin/$', views.signin, name='signin'),
    url(r'^signout/$', views.signout, name='signout'),
    url(r'^account/$', views.account, name='account'),
    url(r'^account/settings/$', views.account_settings, name='account_settings'),
    url(r'^account/change/password/$', 
        views.account_change_password, name='account_change_password'),
    # url(r'^account/(?P<username>\w+)/$', views.account, name='account'),
]