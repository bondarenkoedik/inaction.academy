import django.conf

from django import forms
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from functools import wraps

from .forms import (SignUpForm, SignInForm,
                    AccountSettings, AccountPasswordSettings)
from .settings import STATIC_TEXT

def logout_required(func):
    @wraps(func)
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect(reverse(django.conf.settings.INDEX_VIEW))
        else:
            return func(request, *args, **kwargs)
    return wrapper

@logout_required
def signup(request):
    if request.method == 'POST':
        form = SignUpForm(data=request.POST)
        if form.is_valid():
            user = User(username=form.cleaned_data['username'],
                        first_name=form.cleaned_data['first_name'],
                        last_name=form.cleaned_data['last_name'],
                        email=form.cleaned_data['email'])
            user.set_password(form.cleaned_data['password'])
            user.save()
            messages.success(request, STATIC_TEXT['successfully_registered'])
            return redirect('signin')
    else:
        form = SignUpForm()
    
    return render(request, 'user/signup.html', {'form': form})


@logout_required
def signin(request):
    if request.method == 'POST':
        form = SignInForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect(request.GET.get('next') or 'account')
                else:
                    messages.error(request, STATIC_TEXT['account_is_disabled'])
            else:
                messages.error(request, STATIC_TEXT['incorrect_username_or_password'])
    else:
        form = SignInForm()
    
    return render(request, 'user/signin.html', {'form': form})


def signout(request):
    logout(request)
    return redirect('index')


@login_required
def account(request, username=None):
    
    if username is None:
        username = request.user.username
    
    # show username account
    return render(request, 'user/account.html')


@login_required
def account_settings(request):

    if request.method == 'POST':
        form = AccountSettings(data=request.POST, user=request.user)
        if form.is_valid():
            user = User.objects.get(id=request.user.id)
            assert user
            if user:

                user.username = form.cleaned_data['username']
                user.first_name = form.cleaned_data['first_name']
                user.last_name = form.cleaned_data['last_name']
                user.email = form.cleaned_data['email']
                user.save()

                update_session_auth_hash(request, user)
                messages.success(request, STATIC_TEXT['changes_were_saved'])
    else:
        initial = dict(
            username=request.user.username,
            first_name=request.user.first_name,
            last_name=request.user.last_name,
            email=request.user.email,
        )
        form = AccountSettings(initial=initial)

    return render(request, 'user/account_settings.html', {'form': form})


@login_required
def account_change_password(request):

    if request.method == 'POST':
        form = AccountPasswordSettings(data=request.POST, user=request.user)
        if form.is_valid():

            password = form.cleaned_data['password']
            repeat_password = form.cleaned_data['repeat_password']

            user = User.objects.get(username=request.user.username)
            user.set_password(password)
            user.save()
            update_session_auth_hash(request, user)
            messages.success(request, STATIC_TEXT['password_successfully_changed'])
    else:
        form = AccountPasswordSettings()

    return render(request, 'user/account_change_password.html', {'form': form})






