from django import forms
from django.contrib.auth.models import User
from django.core.validators import RegexValidator

from user.settings import STATIC_TEXT


class PasswordField(forms.CharField):

    def __init__(self, *args, **kwargs):
        kwargs['widget'] = forms.PasswordInput()
        kwargs['min_length'] = 4
        super().__init__(*args, **kwargs)


class UsernameField(forms.CharField):

    def __init__(self, *args, **kwargs):
        kwargs['help_text'] = ''
        kwargs['min_length'] = 4
        super().__init__(*args, **kwargs)

class WorldField(forms.CharField):

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 30
        kwargs['validators'] = [
            RegexValidator(
                r'^[a-zA-Z]+$',
                'Only alphabet letters'
            )
        ]
        super().__init__(*args, **kwargs)


class UserData(forms.Form):
    
    username = UsernameField()
    first_name = WorldField()
    last_name = WorldField()
    email = forms.EmailField()


class SignUpForm(UserData):

    password = PasswordField()
    repeat_password = PasswordField()

    def clean_username(self):

        username = self.cleaned_data['username']

        if User.objects.filter(username=username).exists():
            raise forms.ValidationError(STATIC_TEXT['user_with_such_username_already_exists'])

        return username

    def clean_email(self):

        email = self.cleaned_data['email']

        if User.objects.filter(email=email).exists():
            raise forms.ValidationError(STATIC_TEXT['user_with_such_email_already_exists'])

        return email

    def clean(self):

        cleaned_data = super().clean()

        password = cleaned_data.get('password')
        repeat_password = cleaned_data.get('repeat_password')

        if password and repeat_password and password != repeat_password:
            raise forms.ValidationError('Passwords should match!')

        return cleaned_data


class SignInForm(forms.Form):

    username = UsernameField()
    password = forms.CharField(widget=forms.PasswordInput())


class AccountSettings(UserData):

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

    def clean_username(self):

        username = self.cleaned_data['username']

        if self.user and username == self.user.get_username():
            return username
        elif User.objects.filter(username=username).exists():
            raise forms.ValidationError(STATIC_TEXT['user_with_such_username_already_exists'])

        return username

    def clean_email(self):

        email = self.cleaned_data['email']

        if self.user and email == self.user.email:
            return email
        elif User.objects.filter(email=email).exists():
            raise forms.ValidationError(STATIC_TEXT['user_with_such_email_already_exists'])

        return email


class AccountPasswordSettings(forms.Form):

    old_password = PasswordField()
    password = PasswordField()
    repeat_password = PasswordField()

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

    def clean_old_password(self):

        old_password = self.cleaned_data['old_password']

        if self.user and not self.user.check_password(old_password):
            raise forms.ValidationError(STATIC_TEXT['old_password_is_not_correct'])

        return old_password


    def clean(self):

        cleaned_data = super().clean()

        password = cleaned_data.get('password')
        repeat_password = cleaned_data.get('repeat_password')

        if password and repeat_password and password != repeat_password:
            raise forms.ValidationError(STATIC_TEXT['passwords_should_match'])

        return cleaned_data

